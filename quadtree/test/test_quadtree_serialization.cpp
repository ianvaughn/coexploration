/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by llindzey on Jan 28, 2020
//

#include <gtest/gtest.h>
#include <quadtree/quadtree.h>
#include <quadtree/quadtree_serialization.h>

#include <ds_acomms_msgs/ModemDataRequest.h>

void TestSubgridsEqual(quadtree::Subgrid& subgrid1, quadtree::Subgrid& subgrid2) {
  ASSERT_EQ(subgrid1.level, subgrid2.level);
  ASSERT_EQ(subgrid1.tree_level, subgrid2.tree_level);
  ASSERT_EQ(subgrid1.timestamp, subgrid2.timestamp);
  ASSERT_EQ(subgrid1.xx, subgrid2.xx);
  ASSERT_EQ(subgrid1.yy, subgrid2.yy);
  ASSERT_EQ(subgrid1.tree_xx, subgrid2.tree_xx);
  ASSERT_EQ(subgrid1.tree_yy, subgrid2.tree_yy);
  ASSERT_EQ(subgrid1.data, subgrid2.data);
  ASSERT_EQ(subgrid1.IsSmall(), subgrid2.IsSmall());
}

TEST(QuadTreeSerializationTest, test_int8_serialization) {
  std::vector<uint8_t> data;
  bool success;
  int value;
  for (int ii = std::numeric_limits<int8_t>::min();
       ii <= std::numeric_limits<int8_t>::max(); ii++) {
    data.clear();
    success = serialize8(ii, &data);
    ASSERT_TRUE(success);
    int index = 0;
    value = deserialize8(data, &index);
    ASSERT_EQ(value, ii);
  }

  data.clear();
  ASSERT_FALSE(serialize8(std::numeric_limits<int8_t>::min() - 1, &data));
  data.clear();
  ASSERT_FALSE(serialize8(std::numeric_limits<int8_t>::max() + 1, &data));
}

TEST(QuadTreeSerializationTest, test_uint8_serialization) {
  std::vector<uint8_t> data;
  bool success;
  int value;
  for (int ii = std::numeric_limits<uint8_t>::min();
       ii <= std::numeric_limits<uint8_t>::max(); ii++) {
    data.clear();
    success = serialize8u(ii, &data);
    ASSERT_TRUE(success);
    int index = 0;
    value = deserialize8u(data, &index);
    ASSERT_EQ(value, ii);
  }

  data.clear();
  ASSERT_FALSE(serialize8u(std::numeric_limits<uint8_t>::min() - 1, &data));
  data.clear();
  ASSERT_FALSE(serialize8u(std::numeric_limits<uint8_t>::max() + 1, &data));
}


TEST(QuadTreeSerializationTest, test_int16_serialization) {
  std::vector<uint8_t> data;
  bool success;
  int value;
  for (int ii = std::numeric_limits<int16_t>::min();
       ii <= std::numeric_limits<int16_t>::max(); ii++) {
    data.clear();
    success = serialize16(ii, &data);
    ASSERT_TRUE(success);
    int index = 0;
    value = deserialize16(data, &index);
    ASSERT_EQ(value, ii);
  }

  data.clear();
  ASSERT_FALSE(serialize16(std::numeric_limits<int16_t>::min() - 1, &data));
  data.clear();
  ASSERT_FALSE(serialize16(std::numeric_limits<int16_t>::max() + 1, &data));
}

// I wish I knew how to iterate over types in C++ ... then this could be one
// test rather than 3.
// TODO: Should also test the uint versions of these ...
TEST(QuadTreeSerializationTest, test_int32_serialization) {
  std::vector<uint8_t> data;
  bool success;
  int value;

  for (int64_t ii = std::numeric_limits<int32_t>::min();
       ii <= std::numeric_limits<int32_t>::min() + 5; ii++) {
    data.clear();
    success = serialize32(ii, &data);
    ASSERT_TRUE(success);
    int index = 0;
    value = deserialize32(data, &index);
    ASSERT_EQ(value, ii);
  }

  // Careful -- 4 byte int will wrap here, making the loop never terminate.
  for (int64_t ii = std::numeric_limits<int32_t>::max() - 5;
       ii <= std::numeric_limits<int32_t>::max(); ii++) {
    data.clear();
    success = serialize32(ii, &data);
    ASSERT_TRUE(success);
    int index = 0;
    value = deserialize32(data, &index);
    ASSERT_EQ(value, ii);
  }

  // These tests don't make sense because default is int32, so +/- wraps
  //  data.clear();
  //  ASSERT_FALSE(serialize32(std::numeric_limits<int32_t>::min() - 1, &data));
  //  data.clear();
  //  ASSERT_FALSE(serialize32(std::numeric_limits<int32_t>::max() + 1, &data));
}


TEST(QuadTreeSerializationTest, test_subgrid_serialization_small1) {
  // Test round-tripping of a subgrid (all values are 0-127)
  float center_x = 5.0;
  float center_y = -515.0;
  int level = 2;
  // For serialization test, doesn't matter if subgrid and tree centers
  // are consistent.
  int tree_level = 8;
  float tree_center_x = 15;
  float tree_center_y = -500;
  uint32_t timestamp = 1576291001;
  quadtree::Subgrid subgrid1(tree_level, tree_center_x, tree_center_y,
			     level, center_x, center_y, timestamp);
  for (int ii = 64; ii < 64 + 64; ii++) {
    subgrid1.data.push_back(ii);
  }
  ASSERT_TRUE(subgrid1.IsSmall());
  ds_acomms_msgs::ModemDataRequest::Response response;
  serializeSubgrid(subgrid1, &response.data);

  quadtree::Subgrid subgrid2;
  deserializeSubgrid(response.data, &subgrid2);
  TestSubgridsEqual(subgrid1, subgrid2);
}


TEST(QuadTreeSerializationTest, test_subgrid_serialization_small2) {
  // Test roundtripping of subgrid, with range of values < 127
  float center_x = 5.0;
  float center_y = -515.0;
  int level = 2;
  // For serialization test, doesn't matter if subgrid and tree centers
  // are consistent.
  int tree_level = 8;
  float tree_center_x = 15;
  float tree_center_y = -500;
  uint32_t timestamp = 1576291001;
  quadtree::Subgrid subgrid1(tree_level, tree_center_x, tree_center_y,
			     level, center_x, center_y, timestamp);
  for (int ii = 64; ii < 255; ii=ii+3) {
    subgrid1.data.push_back(ii);
  }
  ASSERT_TRUE(subgrid1.IsSmall());
  ds_acomms_msgs::ModemDataRequest::Response response;
  serializeSubgrid(subgrid1, &response.data);

  quadtree::Subgrid subgrid2;
  deserializeSubgrid(response.data, &subgrid2);
  TestSubgridsEqual(subgrid1, subgrid2);
}


TEST(QuadTreeSerializationTest, test_subgrid_serialization_not_small1) {
  // Test round-tripping of a subgrid
  float center_x = 5.0;
  float center_y = -515.0;
  int level = 2;
  // For serialization test, doesn't matter if subgrid and tree centers
  // are consistent.
  int tree_level = 8;
  float tree_center_x = 15;
  float tree_center_y = -500;
  uint32_t timestamp = 1576291001;
  int min_val = 900;
  int max_val = 1924;
  int step = 16;
  quadtree::Subgrid subgrid1(tree_level, tree_center_x, tree_center_y,
			     level, center_x, center_y, timestamp);
  for (int ii = min_val; ii < max_val; ii=ii+step) {
    subgrid1.data.push_back(ii);
  }
  ASSERT_FALSE(subgrid1.IsSmall());
  ds_acomms_msgs::ModemDataRequest::Response response;
  serializeSubgrid(subgrid1, &response.data);

  quadtree::Subgrid subgrid2;
  deserializeSubgrid(response.data, &subgrid2);
  TestSubgridsEqual(subgrid1, subgrid2);
}


TEST(QuadTreeSerializationTest, test_subgrid_serialization_not_small2) {
  // Test round-tripping of a subgrid
  float center_x = 5.0;
  float center_y = -515.0;
  int level = 2;
  // For serialization test, doesn't matter if subgrid and tree centers
  // are consistent.
  int tree_level = 8;
  float tree_center_x = 15;
  float tree_center_y = -500;
  uint32_t timestamp = 1576291001;
  uint min_val = 0;
  uint max_val = 64000;
  uint step = 1000;
  quadtree::Subgrid subgrid1(tree_level, tree_center_x, tree_center_y,
			     level, center_x, center_y, timestamp);
  for (int ii = 0; ii < max_val; ii=ii+step) {
    subgrid1.data.push_back(ii);
  }
  ASSERT_FALSE(subgrid1.IsSmall());
  ds_acomms_msgs::ModemDataRequest::Response response;
  serializeSubgrid(subgrid1, &response.data);

  quadtree::Subgrid subgrid2;
  deserializeSubgrid(response.data, &subgrid2);
  TestSubgridsEqual(subgrid1, subgrid2);
}


TEST(QuadTreeSerializationTest, test_subgrid_serialization_invalid_data1) {
  // Test round-tripping of subgrid with large range and invalid values
  float center_x = 5.0;
  float center_y = -515.0;
  int level = 2;
  // For serialization test, doesn't matter if subgrid and tree centers
  // are consistent.
  int tree_level = 8;
  float tree_center_x = 15;
  float tree_center_y = -500;
  uint32_t timestamp = 1576291001;
  quadtree::Subgrid subgrid1(tree_level, tree_center_x, tree_center_y,
			     level, center_x, center_y, timestamp);
  for (int ii = 0; ii < 64000; ii=ii+1000) {
    subgrid1.data.push_back(ii);
  }
  // Add invalid-data flag randomly
  for (int idx = 10; idx < subgrid1.data.size(); idx += 15) {
    subgrid1.data.at(idx) = std::numeric_limits<int>::max();
  }
  ASSERT_FALSE(subgrid1.IsSmall());
  ds_acomms_msgs::ModemDataRequest::Response response;
  serializeSubgrid(subgrid1, &response.data);

  quadtree::Subgrid subgrid2;
  deserializeSubgrid(response.data, &subgrid2);
  TestSubgridsEqual(subgrid1, subgrid2);
}


TEST(QuadTreeSerializationTest, test_subgrid_serialization_invalid_data2) {
  // Test round-tripping of subgrid with small range and invalid values
  float center_x = 5.0;
  float center_y = -515.0;
  int level = 2;
  // For serialization test, doesn't matter if subgrid and tree centers
  // are consistent.
  int tree_level = 8;
  float tree_center_x = 15;
  float tree_center_y = -500;
  uint32_t timestamp = 1576291001;
  quadtree::Subgrid subgrid1(tree_level, tree_center_x, tree_center_y,
			     level, center_x, center_y, timestamp);
  for (int ii = 64; ii < 64 + 64; ii++) {
    subgrid1.data.push_back(ii);
  }
  // Add invalid-data flag randomly
  for (int idx = 10; idx < subgrid1.data.size(); idx += 15) {
    subgrid1.data.at(idx) = std::numeric_limits<int>::max();
  }
  ASSERT_TRUE(subgrid1.IsSmall());
  ds_acomms_msgs::ModemDataRequest::Response response;
  serializeSubgrid(subgrid1, &response.data);

  quadtree::Subgrid subgrid2;
  deserializeSubgrid(response.data, &subgrid2);
  TestSubgridsEqual(subgrid1, subgrid2);
}


int main(int argc, char** argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
