
\documentclass[]{article}

\usepackage{graphicx}
\usepackage{hyperref} % for \url
\usepackage{verbatim}
\usepackage{amsmath}
\usepackage[margin=1in]{geometry}

\setlength{\parindent}{0pt}

%opening
\title{Multibeam Mapping \& Transfer}
\author{Laura Lindzey}

\begin{document}
	
	\maketitle
	
\section{Overview}
	
	The CoExploration project requires the ability to transfer a representation of the bathymetry data collected by a sea-floor mapping AUV during a dive, in order for topside scientists to target follow-up camera surveys later in the same dive. 
	
	This will require developing a number of new capabilities:  
	\begin{enumerate}
		\item Real-time point cloud generation from sonar data (Section~\ref{sec:multibeam_filtering})
		\item Real-time map generation from the sonar data (Section~\ref{sec:map_generation})
		\item Acoustic transmission of the resulting map (Section~\ref{sec:map_transfer})
	\end{enumerate}
	
	
\section{Multibeam Point Clouds}
\label{sec:multibeam_filtering}

(NB: This entire section was informed by conversations with Ian, and primarily reflects an architecture that he has been working towards.)

We do not currently have the capability to publish real-time sonar data as ROS messages.
There are two parts to this: a driver that publishes a standardized raw message, and a filtering pipeline that is the equivalent of (but hopefully better than!) mbclean.
The output of that filtering pipeline will be a \texttt{sensor\_msgs/PointCloud2}.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=0.9\textwidth]{MultibeamDataFlow.png}
	\caption{Data flow for live and post-processed multibeam pipelines.}
	\label{fig:multibeam_pipeline}
\end{figure}

There is an additional desire to have the automated pipeline be compatible with the post-processing pipeline.
Figure~\ref{fig:multibeam_pipeline} shows the desired data flow through both pipelines.
There are a few subtleties:
\begin{itemize}
	\item mbsystem and commercial tools all expect the pings to be in a local-level reference frame at the surface of the water. Local-level makes sense because raytracing calculations are dependent on depth, but will be rotationally symmetric with heading. At-surface is because they all ignore AUVs. ROS convention would be to have the center of the ping point cloud at the sensor frame, and octomap requires this for its own raytracing. We will define another frame that is local-level centered at the multibeam and use this for the output of ds\_mbfilt.
	\item There is concern over excess bandwidth and logging, particularly if we operate at lower altitudes. This could be addressed by moving the filtering and gridding into the same process by use of nodelets, but is only worth the programmer time if we're hitting computer limits.
	\item Traditional multibeam processing does not throw out any points, it just flags them. We wish to preserve this capability, and the ability to examine what part of the pipeline flagged each point (sensor, automatic filtering, manual cleaning).
	\item Ian wants to be able to feed the output of the manual processed pipeline (they tend to excel at ping editing) into the real-time gridder, since this will enable development of gridding and SLAM algorithms without having perfected the filtering. There is some trickiness in this because the output files do not include depth; this will have to be added back in via tf (potentially, we should publish the transformation between multibeam-local-level and surface-local-level, but that would require both pressure sensor data and the tide model.) 
\end{itemize}

\subsection{Drivers / publications}

For the DSL sensors that we are likely to use:
\begin{itemize}
	\item Reson (legacy Sentry) - Ian has converted one dive's worth of data from s7k to a bagfile that contains \texttt{ds\_multibeam\_msgs/MultibeamRaw} messages. Section~\ref{sec:reson_conversion} describes this process.
	\item Kongsberg (new Sentry) - a ROS driver exists, and it publishes \texttt{ds\_multibeam\_msgs/MultibeamRaw} messages (however, without the correct header information). The quadtree has been tested with a bagfile from the engineering cruise.
	\item Norbit - This driver has been written and tested on the Pontoon of Science. It also publishes \texttt{MultibeamRaw} messages.
\end{itemize}

\subsection{Reson Conversion (s7k $\rightarrow$ rosbag)}
\label{sec:reson_conversion}

There's a somewhat hacky/convoluted but totally functional way to generate bagfiles from s7k files and the rest of sentry's logs.
(Ian implemented this; my only contribution is figuring it out and documenting it.)

First, convert the s7k to a bagfile containing only MultibeamRaw:
\begin{itemize}
	\item roscore
	\item rosbag record -O sentry506/reson\_only /s7k\_replay/multibeam
	\item  rosrun ds\_reson s7k\_replay \_s7k\_directory:=2018-cordes/dives/sentry506/multibeam/raw \_realtime:=False
\end{itemize}
(On my laptop, this takes 30 minutes for an 8-hour dive. NB: s7k\_replay doesn't terminate, it just stops publishing.)

Next, merge that bag with the rest of the ROS logs from the dive. 
Ian has scripts to do this that are on the dav (/users/ivaughn/mbproc-live); I updated them to look for logs based on input cruise/dive.
\begin{itemize}
	\item cd mbproc-live; python merge\_bags.py
\end{itemize}
The resulting bagfile contains all of the messages required to test multibeam map creation and transfer.

\subsection{ROS filtering pipeline}

Ian has implemented a subset of mbsystem's filtering capability within ds\_multibeam.
This subscribes to \texttt{MultibeamRaw} messages, and publishes both the raw and filtered pings as \texttt{PointCloud} messages (one message per ping).

It currently filters based on:
\begin{itemize}
	\item Whether the multibeam instrument flagged it
	\item Range (sensor to return): min / max / max allowed jump
	\item Altitude (vertical distance below sensor): min / max / max jump
	\item Depth (absolute): min / max / max jump
	\item Dana's backup metric: working from the center of the beam outward, all points should have increasing y coordinate in the sensor's frame.
\end{itemize}

Ian has also been funded to work on ML-based classifiers for multibeam. 
The timeline of that project is not expected to be compatible with the initial CoExploration demos, but anything he develops is expected to be immediately applicable to this work.

\subsection{Proposed Work}

With minor changes, the existing filtering implementation in \texttt{ds\_multibeam} will work for both CoExploration and other projects that are planning to use Octomap:
\begin{itemize}
	\item Output of filtering should be published as PointCloud2 message in the multibeam's frame. (ROS convention is for the consuming node to transform into whatever frame is required.)
	\item Split ray-tracing, filtering and gridding into two separate nodes (ray-tracing + filtering, gridding), so different gridding approaches can be used. They were originally a single node due to concerns about network traffic and log sizes. If this becomes a problem, it should be possible to turn them into nodelets which enables ROS pub/sub to utilize shared memory. Similarly, if any processes wind up wanting the unfiltered but ray-traced pointclouds, ray-tracing could be split out from filtering.
\end{itemize}

\section{Map Generation / Representation}
\label{sec:map_generation}

\subsection{2.5D Growable Grid}

Ian has implemented an almost as-simple-as-possible gridder that takes the output of the filtered pings and creates a 2.5D growable grid representation.
The map is represented as a flat array, with functions for converting between indices and northing/easting coordinates. 
For each cell, reported depth is the average of all observations that are in that cell's x/y boundary, implemented with an accumulator and a count. 
If a new point is outside the bounds of the existing grid, the map will be resized if possible, up to a fixed maximum size in memory. 


\subsection{Octomap}

The octomap library and associated ROS wrappers allow out-of-the-box 3D mapping. \url{http://wiki.ros.org/octomap_server}
It uses a probabilistic voxel grid to explicitly represent both obstacles and known free space.
The algorithm and implementation have been optimized for run-time memory usage, explained in detail in  \cite{Hornung2013octomap}.

In order to integrate Octomap with our current filtering pipeline, we would simply need to publish each ping as a PointCloud2 message in the sensor's frame. (The current setup transforms them to odom\_dr before publishing, which interferes with the ray-tracing used to update free space.)

This is appealing because it would give us a grid out-of-the-box, but for a number of reasons it isn't particularly well suited to our use case:
\begin{itemize}
	\item Octomap is based on probabilistic occupancy grids, which work best when there will be multiple observations of the same voxel. This is typically not the case in multibeam survey work.
	\item The voxels are cubes; it is not possible to have vertical resolution different from horizontal resolution.
	\item It is currently limited to a fixed region around the origin
	\item While there are tools to project the occupied cells to a 2D map, this is based on a slice at a given z-value (as would be useful for a ground robot operating in a level environment.) It does not already have utilities to project down to the 2.5D map that we want.
\end{itemize}

\subsection{Quadtree-based approach}

A quadtree is a natural data structure for storing a 2.5D grid where the extent is not known a-priori, or where the shape of the area does not approximate an axis-aligned grid.
There are performance and implementation tradeoffs for using a quadtree vs a flat grid.

In a quadtree, each cell contains the data as well as pointers to its children.
For our use case, the crossover point for whether a quadtree or array is more memory efficient occurs when approximately half of the grid cells would have data.
While either would work in typical NDSF operations (1-4~m cell size, compact survey plan, $>$250~MB memory availble) the quadtree performs better as we deviate from these assumptions.

More importantly for this project, the quadtree naturally represents the topography in a hierarchical data structure that makes it simple to query lower-resolution maps and determine which subtrees have been updated and need to be re-transmitted.

\section{Map Transfer}
\label{sec:map_transfer}

A number of approaches have been proposed to handle map transfer; this section explores the options.
Some of these could accept the existing output of the grid generation; others would require additional features.

Requirements (and nice-to-haves) for multibeam map transfer:
\begin{itemize}
	\item Transmit data in encoded chunks of $\le$(256 - 8) bytes. (Micromodem frame is 256 bytes; comms manager currently requires 8 bytes for header, but that may be reduced.)
	\item Robust to dropped frames / packets. 
	\item Topside user able to request higher resolution of given region.
	\item Support updating map as new data is collected (whether this be with incremental updates to the transmitted representation, or completely overwriting tiles)
	\item Grid size / location not fixed at start. (This is in support of mid-dive retargeting; may not know spatial extent of mission at launch time.)
	\item Robust to excessively large map (e.g., should not crash / consume all of stack's memory if the state estimate drifts catastrophically.)
\end{itemize}

\subsection{Progressive Imagery}
\label{sec:progressive_imagery}

At this point, the default option is to use Progressive Imagery to transmit tiles of a generated grid.

This assumes that multibeam processing functionality described in Sections~\ref{sec:multibeam_filtering}~\&~\ref{sec:map_generation} has already been implemented:
\begin{itemize}
	\item Instrument publishes raw multibeam data of type \texttt{ds\_multibeam\_msgs/MultibeamRaw}
	\item Filtering is applied to raw multibeam data, and the resulting points are published as \texttt{sensor\_msgs/PointCloud2}
	\item A gridder is running that subscribes to the point clouds and \texttt{/tf}, and publishes the resulting grid as either a \texttt{ds\_multibeam\_msgs/MultibeamGrid} or a \texttt{sensor\_msgs/PointCloud2} message.
\end{itemize}
(So, there is still some outstanding work required independent of actual implementation of the transfer.)

Using Progressive Imagery would require implementing a number of nodes:
\begin{itemize}
	\item \textbf{grid\_splitter}: Subscribe to the output of the gridder; when requested by a user (msg or srv call), split the most recent grid into tiles, which are saved in tif format in the Progressive Imagery transmit directory. The filename should encode that it is an image due to the grid, information about where in the grid the tile belongs, and which version of that tile it is. 
	\item \textbf{grid\_gui}: Topside GUI (standalone, or NavG3 plugin) that assembles the transferred tiles to show the full map. Automatically updates display as tile images are transferred by progressive imagery. Hovering over a tile indicates the full image ID, which allows the operator to use the Progressive Imagery GUI to prioritize different tiles. Additionally, it will allow the user to see what versions of tiles are available, and flip between them on a per-tile basis (There is no reason to look at the newer version of a tile that has not changed since the previous tiling, since less of it resolution will have been transferred.)
\end{itemize}

The weakness of this approach is the complexity introduced by treating a continually-updated grid as a static, snapshotted image. 
Managing that complexity is offloaded to the operator, leading to a clunky UX.
Additionally, this is probably suboptimal in terms of bandwidth, since previous versions of the map cannot be built upon or updated -- at best, the operator manually instructs Progressive Imagery to deprioritize transferring tiles that are not expected to have new sonar data.

Open Questions:
\begin{itemize} 
	\item How well does PI perform on gridded bathymetry? Given a small-enough input image, will 100\% transfer correspond to pixel-perfect values?
	\item How many total bytes/frames are transferred for a given tile resolution? 
	\item How to handle NaN grid values? Interpolate before sending? Set to 0 or MAX\_INT and expect artifacts? Send separate mask image?
	\item Does PI handle 16-bit tiffs? Or does it convert down to 8-bit greyscale?
\end{itemize}


\subsection{Other Image Compression}
\label{sec:image_compression}

For some types of gridded data, other compression formats are more appealing than the JPEG2000 that PI is based around.
e.g. for categorical labels, PNG will be better, since averaging category 1 and category 3 will probably not be semantically related to category 2. 
Additionally, PNG is efficient at compressing large contiguous regions with the same value, as we would expect to have around the edges of a sonar map.

However, traditional image-based compression all suffers from the same weaknesses as Progressive Imagery -- they do not support updating the image incrementally.
Additionally, the requirement that chunks fit into a single micromodem frame means that header-heavy compression schemes are very suboptimal, as the header/payload ratio becomes ridiculous at small packet sizes.


\subsection{Grid Transfer}

While the implementation details depend on which option for online gridding is chosen from Section~\ref{sec:map_generation}, this section discusses the interfaces (API and GUI) that would support explicitly transferring the grid values.

The advantage of this type of approach is that it enables incremental updates, where each update includes the most recent data and a subgrid is only re-sent if it has been updated.

% TODO: Ian wants a metric for evaluating the grid transfer performance. (There's a gap between talking about "best possible" to "how did we do"). He was thinking about entropy between ideal map and the representation topside; 


\subsubsection{API}

\paragraph{Map Metadata}
A separate map header message will be periodically published (1/minute?) that includes: map origin in meters relative to ??? and time in seconds since the epoch (x0, y0, t0). This will not change throughout the dive.

\paragraph{Input to Gridding}
The subsea map will be updated in response to PointCloud2 messages, with details discussed in a previous section.

\paragraph{Map Updates}

The topside map will be created from updates from the subsea map, that consist of:
\begin{itemize}
	\item int16 dx0, dy0: origin of the sub-grid being transferred, relative to the map origin, in cells. Each is int16 (for a total of 4 bytes), giving $\pm$32~km from the origin at 1~m grid resolution. (At 1~m/s, that is 9~hours of straight-line swimming.) This defines the south and west boundaries of the subgrid, not the center of the most southwest cell.
	\item uint16 dt: Time of most recent sonar reading contributing to this sub-grid, expressed as offset from map origin time. uint16 would support an 18~hour dive.
	\item cell\_size: uint8, enum. Allowable values are 1, 2, 4, 8, 16, 32, 64 meters.
	\item uint16 data[]: depth values for each cell in the grid, arranged in row order. This allows for 10~cm vertical resolution at up to 6.4~km depths. 0 is the no-data value. 
\end{itemize}

Related tradeoffs / decisions:
\begin{itemize}
	\item Fixed subgrid size, or specified in the message? I am choosing to go with a fixed subgrid size for simplicity. I don't want to solve the knapsack problem for partitioning map updates. Relatedly, the subgrid boundaries are pre-defined. This has the biggest impact in areas where the coverage is on the order of the subgrid size (in the theoretical worst-case, could require 4x bandwidth for single cell; but that impact decreases as the map area : subgrid area ratio increases.) 
	\item Should the subgrid sizes be maximal (10x12 cells), or evenly-divisible (8x8 cells)? This may depend on implementation details (particularly whether the maps are represented as arrays or quadtrees, and whether some messages are reduced to uint8 data.)
	\item If the subgrid has little enough vertical variation ($<\sim$24.5~m), should it be transmitted as uint8 instead? (This would add a bool flag to the messages.)
	\item is a uint16 dt OK, or should we go with the uint32 time? This partially depends on whether or not we try to absolutely maximize subgrid size (to 120 total cells) or if we go with 64 cells, since that affects how many bytes are available for the subgrid header.
	\item IF we go with 8x8 subgrids, and add a flag for offset/datatype, should we also support data requests for highest-priority uint8 updates, rather than relying on chance for whether the next one is uint8/uint16? (I'd be inclined to not try to optimize this too much -- without constraints, just choose next with priority. With constraints, look for small one.)
	\item According to Ian, the 32~km limit on map size has been hit (rarely). There are a number of possible ways to address this: decide it's an unsupported case; go ahead and increase the number of bytes transmitted in every message; degrade to lower-resolution map (e.g.if a point is too far away at 1~m resolution, only send updates to the 2~m cells). 
\end{itemize}

Open Questions:
\begin{itemize}
	\item What is the typical vertical variation at different length scales for the maps Sentry collects? (How often would it be possible to reduce depth to uint8 + uint16 offset, rather than uint16? If only a small fraction of transmissions would be able to use uint8, there's no reason to add the complexity of switching between them.) This would be easy enough to compute by loading the final multibeam grids from a bunch of dives and computing min/max of the values for 8x8 grids of different cell sizes. 
\end{itemize}

\paragraph{Prioritized Updates}

The subsea mapping node will implement a ROS service, with the standardized background data request interface. 
In the absence of user-requested priorities, data will be streamed without any acks/nacks. 
Data will only be re-sent if explicitly requested, but the streaming should avoid re-sending a subgrid if it has been implicitly acked in a user request.
Streaming will start with the coarsest cells (32~m cells -- coarser makes no sense, given expected multibeam mapping coverage rates) working down to the finest (4~m -- streaming finer data doesn't make sense because even in ideal conditions the micromodem couldn't keep up with the rate at which it is typically generated).

Each subgrid will only be transmitted if it both has data and hasn't been updated for a given time, where the maturation time depends on how long the AUV is expected to take to fully traverse the subgrid. 
This prevents the same subgrid from updating repeatedly on the same transect. 
For example, an 8x8 grid of 32~m cells would take $\sim$6~minutes to traverse on the diagonal, making that a reasonable first-cut for that delay time.
With smaller subgrids, I'd expect reasonable delay times to be governed more by how often cells are re-surveyed at the sides of scans based on vehicle yaw (as a WAG, 1 or 2 minutes is probably sufficient for most situations.)

For a given grid resolution, subgrids meeting the delay time requirement will be prioritized based on which one has the longest gap between the current time and the time it was last updated. 

Prioritized updates will be requested with a message consisting of an array of subgrid descriptors:
\begin{itemize}
	\item int16 dx0, dy0: origin of sub-grid, in cells
	\item uint8 cell\_size
	\item uint16 dt: timestamp for most recently-received data from this subgrid. 0 if has not yet been received. This allows the subsea sending node to determine whether it has new data, and to save bandwidth if the update would be extraneous. 
\end{itemize}
Encoded well, this would fit a request for 35 subgrids into a message that will fit into a single frame. (The Comms Manager message encoding is not yet that efficient.)

Engineering Decisions:
\begin{itemize}
	\item Newer update requests are inserted in the queue before older ones, so they are guaranteed to be sent (otherwise, could wind up with situation where requests are being sent faster than they can be transmitted, and there's an increasing, unbounded, delay.) Queue size is limited, and when new requests arrive, any duplicates (or subsumed ones, with the same subgrid but a later dt) are deleted.
	\item How will the streaming updates be prioritized, once they meet the delay criteria? Would it be better to base it on percentage changed since the last transmission? 
	\item are prioritized requests pushed or pulled to the comms manager? Are they a background data service (like Progressive Imagery), or are they a separate category of message transmissions where any messages received by the comms manager need to be transmitted?
\end{itemize}

Questions:
\begin{itemize}
	\item Should the delay timeout for deciding a subgrid is ready to send be based on cell size (being "finished"), or more-or-less constant, based on how often and how far the vehicle yaws and re-scans existing data?
\end{itemize}

\subsubsection{Lossless Compression}

The approach described in the previous section emphasizes efficient transmission of grids by adaptively changing grid sizes based on the topside operator's interest.
The only compression (for a loose definition of compression) comes in:
\begin{itemize}
	\item (potentially) switching between 8 and 16 bit representations of the transmitted grid cells
	\item sending data with larger grid cells
	\item tracking update times to avoid re-transmitting redundant data
\end{itemize}

For reasons discussed in Sections~\ref{sec:progressive_imagery}\&\ref{sec:image_compression}, a lossy compression scheme is not ideal for this type of data.
However, a lossless approach like that used by zlib could be of interest for compressing the transmitted grids.

On first look, I suspect that the sliding window approach used by the ZL77 algorithm (which is used by zlib) would not obtain significant enough compression on our data on the 256-byte chunks mandated by our usage of the WHOI micromodem.
It might be worth investigating this further with existing grids.
(Another approach would be to get around the 256-byte chunk limit by moving to a nack/ack design like progressive imagery where we assume that all data is eventually transmitted rather than having each packet interpretable on its own. This has the potential to interfere with our goal of "don't transmit out-of-date cells")                                                                                                                                                                                                                                                        

\subsubsection{Topside GUI}

The topside GUI will provide the option to display either highest resolution grid that has been received, or the most recently received data.
These data structures will be cached in order to enable restarting the GUI and topside server during operations without losing any state.


The subsea multibeam server will attempt to send each cell once; the topside user can select a region and resolution to request data be resent. 
This selection will be implemented in the gui by dragging a region.

An obvious extension to this is to automatically generate nacks to trigger re-sending of previously-requested cells.
Automating nacks in the general case would be more complicated, but could enable topside to subsea map transition. 
Since the maps are not guaranteed to be dense grids, topside does not necessarily know which region are missing data. 
One option would to add a sequential packet ID to each message, so topside can detect when they are skipped, and request subsea to re-send. 
This would require the sending node to retain a map of id to coordinates. 
Alternatively, we could implement acks where the sender circles back to any un-acked data before sending the next lower grid resolution.

% NB: Ian also wanted me to talk about how you could be clever and decide not to send higher-resolution grids if a previously-transmitted lower-resolution grid was a sufficiently good approximation. (Ordering by expected information gain, rather than coverage. This is a case where the topside operator may have insufficient information to make that call, so our paradigm of "human decides" may be strictly suboptimal.)


\bibliographystyle{acm}
\bibliography{coexploration}

\end{document}