/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by llindzey on Nov 19, 2019.
//

#include "quadtree/quadtree.h"

namespace quadtree {

QuadTree::QuadTree(float h_res, float v_res, int send_levels)
    : horizontal_resolution_(h_res),
      vertical_resolution_(v_res),
      num_levels_(0),
      send_levels_(send_levels) {
  // These need to come first, since they're used immediately after.
  setLogMessageCallback("fatal", defaultFatalMessageCallback);
  setLogMessageCallback("error", defaultErrorMessageCallback);
  setLogMessageCallback("warn", defaultWarnMessageCallback);
  setLogMessageCallback("info", defaultInfoMessageCallback);
  setLogMessageCallback("debug", defaultDebugMessageCallback);

  if (send_levels_ == 1) {
    warn_("QuadTree: Tree will send 1x1 updates");
  } else if (send_levels <= 0) {
    error_("QuadTree: send_levels must be positive");
    assert(false);
  }
}

void QuadTree::setLogMessageCallback(std::string level, LogMessageCallback cb) {
  if (level == "fatal") {
    fatal_ = cb;
  } else if (level == "error") {
    error_ = cb;
  } else if (level == "warn") {
    warn_ = cb;
  } else if (level == "info") {
    info_ = cb;
  } else if (level == "debug") {
    debug_ = cb;
  } else {
    // If we ever want to set callbacks outside of initialization, this error
    // needs to be handled more gracefully.
    assert(false);
  }
}

void QuadTree::GetLevelOffset(const int level, const int child_index, float* dx,
                              float* dy) {
  float step = powf(2.0, (int)level - 2);

  switch (child_index) {
    case Node::UPPER_LEFT: {
      *dx = -step;
      *dy = step;
      break;
    }
    case Node::UPPER_RIGHT: {
      *dx = step;
      *dy = step;
      break;
    }
    case Node::LOWER_LEFT: {
      *dx = -step;
      *dy = -step;
      break;
    }
    case Node::LOWER_RIGHT: {
      *dx = step;
      *dy = -step;
      break;
    }
    default: { assert(false); }
  }
}

Node* QuadTree::GetNode(float xx, float yy, int level, float* nodex,
			float* nodey) {
  if (level >= num_levels_ || level < 0) {
    std::stringstream ss;
    ss << "GetNode: requested node at level " << level << ", but tree only has "
       << num_levels_ << " levels.";
    warn_(ss.str());
    return nullptr;
  }
  Node* ptr = root_.get();
  int curr_level = num_levels_ - 1;
  auto curr_x = x_center_;
  auto curr_y = y_center_;

  while (curr_level > level) {
    int child_index = GetChildIndex(xx - curr_x, yy - curr_y);
    float dx, dy;
    GetLevelOffset(curr_level, child_index, &dx, &dy);
    curr_x += dx;
    curr_y += dy;
    // Check that input coordinate is within the domain of the current node
    if (std::fabs(curr_x - xx) > std::fabs(dx) ||
	std::fabs(curr_y - yy) > std::fabs(dy)) {
      return nullptr;
    }

    curr_level -= 1;
    // TODO: This should never happen -- all internal nodes have at least
    //       one child. Make an assert instead? Or add error messages?
    // (Errors requires figuring out how to hook up with roscpp's log stream)
    if (!ptr->children) {
      return nullptr;
    }
    if (!ptr->children->at(child_index)) {
      return nullptr;
    }
    ptr = ptr->children->at(child_index).get();
  }
  *nodex = curr_x;
  *nodey = curr_y;
  return ptr;
}


int QuadTree::GetChildIndex(float dx, float dy) {
  int child_index;
  if (dx > 0) {
    if (dy > 0) {
      child_index = Node::UPPER_RIGHT;
    } else {
      child_index = Node::LOWER_RIGHT;
    }
  } else {
    if (dy > 0) {
      child_index = Node::UPPER_LEFT;
    } else {
      child_index = Node::LOWER_LEFT;
    }
  }
  return child_index;
}

void QuadTree::TraverseSubgrid(Node* node, int level,
                               coro_grid_t::push_type& out) {
  // Handle case where this is called before tree is initialized
  if (!node) {
    return;
  }
  if (0 == level) {
    out(node);
    return;
  }
  // TODO: Possible bug here? Shouldn't this output the right number of nullptr's?
  if (!node->children) {
    return;
  }
  for (int idx = 0; idx < Node::NUM_CHILDREN; idx++) {
    if (node->children->at(idx)) {
      TraverseSubgrid(node->children->at(idx).get(), level - 1, out);
    } else {
      for (int ii = 0; ii < pow(pow(2, level - 1), 2); ii++) {
        out(nullptr);
      }
    }
  }
}

void QuadTree::TraverseBFS(Node* node, int level, float xc, float yc,
                           coro_dfs_t::push_type& out) {
 // Handle case where this is called before tree is initialized
  if (!node) {
    return;
  }

  for (int ll = num_levels_ - 1; ll >= 0; ll -= 1) {
    TraverseLevel(ll, node, level, xc, yc, out);
  }

}

  // NB: This implementation is very similar to TraverseDFS...
void QuadTree::TraverseLevel(int target_level, Node* node, int level, float xc,
			     float yc, coro_dfs_t::push_type& out) {

  if (target_level == level) {
    out(std::make_tuple(node, level, xc, yc));
    return;
  }

  if (!node->children) {
    return;
  }

  float dx, dy;
  for (int idx = 0; idx < Node::NUM_CHILDREN; idx++) {
    if (node->children->at(idx)) {
      GetLevelOffset(level, idx, &dx, &dy);
      TraverseLevel(target_level, node->children->at(idx).get(), level - 1,
                    xc + dx, yc + dy, out);
    }
  }
}


void QuadTree::TraverseDFS(Node* node, int level, float xc, float yc,
                           coro_dfs_t::push_type& out) {
  // Handle case where this is called before tree is initialized
  if (!node) {
    return;
  }

  out(std::make_tuple(node, level, xc, yc));
  if (!node->children) {
    return;
  }

  float dx, dy;
  for (int idx = 0; idx < Node::NUM_CHILDREN; idx++) {
    if (node->children->at(idx)) {
      GetLevelOffset(level, idx, &dx, &dy);
      TraverseDFS(node->children->at(idx).get(), level - 1, xc + dx, yc + dy,
                  out);
    }
  }
}

void QuadTree::SaveGrids(std::string output_dir, int min_level, int max_level) {
  for (int level = min_level; level <= max_level; level++) {
    if (level >= num_levels_) {
      continue;
    }
    auto grid = MakeGrid(level);
    if (grid == nullptr || grid->size() == 0) {
      continue;
    }
    // Format is 28-byte header, followed by row-major data bytes
    std::string filename = output_dir + "/level_" + std::to_string(level) + ".bin";
    std::ofstream os(filename, std::ios::out | std::ios::binary);
    int32_t ll = level;
    os.write(reinterpret_cast<char*>(&ll), sizeof(int32_t));

    // TODO: I don't love passing around grid origin & bounds separate from grid data
    float min_x, max_x, min_y, max_y;
    GetTreeExtrema(level, &min_x, &max_x, &min_y, &max_y);
    os.write(reinterpret_cast<char*>(&min_x), sizeof(float));
    os.write(reinterpret_cast<char*>(&min_y), sizeof(float));
    int32_t rows = grid->size();
    int32_t cols = grid->at(0).size();
    os.write(reinterpret_cast<char*>(&rows), sizeof(int32_t));
    os.write(reinterpret_cast<char*>(&cols), sizeof(int32_t));
    os.write(reinterpret_cast<char*>(&horizontal_resolution_), sizeof(float));
    os.write(reinterpret_cast<char*>(&vertical_resolution_), sizeof(float));

    for (int ii = 0; ii < rows; ii++) {
      for (int jj = 0; jj < cols; jj++){
	os.write(reinterpret_cast<char*>(&grid->at(ii).at(jj)), sizeof(int));
      }
    }

    os.close();
  }
}

std::unique_ptr<QuadTree::grid_t> QuadTree::MakeGrid(int grid_level) {
  if (!root_) {
    info_("QuadTree::MakeGrid: Can't make grid for empty tree.");
    return std::make_unique<QuadTree::grid_t>();
  }
  if (grid_level >= num_levels_) {
    std::stringstream ss;
    ss << "QuadTree::MakeGrid: Can't make grid at level " << grid_level
       << ". Tree only has " << num_levels_ << " levels";
    info_(ss.str());
    return std::make_unique<QuadTree::grid_t>();
  }
  // NB: This is in units of level0 grid cells, NOT world coordinate units
  float cell_size = 1.0*pow(2, grid_level);

  float min_x_coord, max_x_coord, min_y_coord, max_y_coord;
  bool success = GetCoordBounds(grid_level, &min_x_coord, &max_x_coord,
				&min_y_coord, &max_y_coord);

  if (!success) {
    // Couldn't compute valid bounds (due to no nodes at that level),
    // so can't make grid.
    debug_("MakeGrid: Couldn't compute valid bounds.");
    return std::make_unique<QuadTree::grid_t>();
  }

  int ncols = max_x_coord/cell_size - min_x_coord/cell_size + 1;
  int nrows = max_y_coord/cell_size - min_y_coord/cell_size + 1;

  auto grid = std::make_unique<QuadTree::grid_t>(
      nrows, std::vector<int>(ncols, std::numeric_limits<int>::max()));

  // Traverse over the tree to fill the grid in
  coro_dfs_t::pull_type traverser([&](coro_dfs_t::push_type& out) {
    // Initializing xc/yc as 0 means that the returned distances will
    // be relative to the tree's center, not absolute.
    TraverseBFS(root_.get(), num_levels_ - 1, 0, 0, out);
  });
  Node* node;
  int level;
  float xc, yc;
  // In C++17 we'll be able to directly unpack the tuple!
  // `for (auto const& [path, status, size] : items) { ...`
  // But, for now, we're stuck with this two-step process.
  for (auto tup : traverser) {
    std::tie(node, level, xc, yc) = tup;
    if (grid_level > level) {
      break;
    }
    if (grid_level == level) {
      int xcoord = std::ceil((xc - min_x_coord) / cell_size);
      int ycoord = std::ceil((yc - min_y_coord) / cell_size);
      if (xcoord < 0 || xcoord >= ncols || ycoord < 0 || ycoord >= nrows) {
	std::stringstream ss;
	ss << "QuadTree::MakeGrid: Invalid coords computed! " << xc << ", "
	   << yc << " at level " << level;
	error_(ss.str());
      }
      grid->at(ycoord).at(xcoord) = node->get_value();
    }
  }
  return grid;
}

bool QuadTree::GetTreeExtrema(int level, float* x_min, float* x_max,
                              float* y_min, float* y_max) {
  *x_min = std::numeric_limits<float>::max();
  *x_max = std::numeric_limits<float>::min();
  *y_min = std::numeric_limits<float>::max();
  *y_max = std::numeric_limits<float>::min();

  coro_dfs_t::pull_type traverser([&](coro_dfs_t::push_type& out) {
      TraverseLevel(level, root_.get(), num_levels_ - 1, x_center_, y_center_, out);
  });

  Node* node;
  int node_level;
  float xx, yy;
  int num_nodes = 0;
  for (const auto& tup : traverser) {
    std::tie(node, node_level, xx, yy) = tup;
    *x_min = std::min(xx, *x_min);
    *x_max = std::max(xx, *x_max);
    *y_min = std::min(yy, *y_min);
    *y_max = std::max(yy, *y_max);
    num_nodes += 1;
  }

  return (num_nodes > 0);
}

bool QuadTree::GetCoordBounds(int level, float* x_min, float* x_max,
                              float* y_min, float* y_max) {
  float x1, x2, y1, y2;
  bool success = GetTreeExtrema(level, &x1, &x2, &y1, &y2);
  if (!success) {
    return false;
  }
  *x_min = x1 - x_center_;
  *x_max = x2 - x_center_;
  *y_min = y1 - y_center_;
  *y_max = y2 - y_center_;
  return true;
}

void QuadTree::CellFromWorld(float xx, float yy, float zz, float* xc, float* yc,
                             float* zc) {
  *xc = xx / horizontal_resolution_;
  *yc = yy / horizontal_resolution_;
  *zc = zz / vertical_resolution_;
}

}  // namespace quadtree
