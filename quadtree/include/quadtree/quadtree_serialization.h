/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by llindzey on Jan 24, 2020.
//

#ifndef DS_QUADTREE_QUADTREE_SERIALIZATION_H
#define DS_QUADTREE_QUADTREE_SERIALIZATION_H

#include <iostream>                 // for std::cout
#include <limits>                   // for std::numeric_limits
#include <vector>

#include <quadtree/quadtree.h>

// How many bytes required to represent the subgrid.
// - 1 byte for level + small/large data
// - 5 byte for int8 tree level + int16 center (x, y)
// - 6 bytes for int16 grid offset and center (x, y)
// - 4 bytes for uint32 timestamp
// - 64 (or 2x64) bytes for the actual data (int8, int16)
// TODO: These constants should be handled by whatever handles serialization
const int kSmallSubgridBytes = 1 + 5 + 3 * 2 + 4 + 64;
const int kSubgridBytes = 1 + 5 + 3 * 2 + 4 + 2 * 64;

bool serializeRequest(const quadtree::GridRequest& request,
		      std::vector<uint8_t>* data);
bool deserializeRequests(const std::vector<uint8_t>& data,
			 std::list<quadtree::GridRequest>* requests);

bool serializeSubgrid(const quadtree::Subgrid& subgrid,
                      std::vector<uint8_t>* data);
bool deserializeSubgrid(const std::vector<uint8_t>& data,
                        quadtree::Subgrid* subgrid);

int32_t deserialize32(const std::vector<uint8_t>& data, int* index);
int16_t deserialize16(const std::vector<uint8_t>& data, int* index);
uint16_t deserialize16u(const std::vector<uint8_t>& data, int* index);
int8_t deserialize8(const std::vector<uint8_t>& data, int* index);
uint8_t deserialize8u(const std::vector<uint8_t>& data, int* index);

bool serialize32(int value, std::vector<uint8_t>* data);
bool serialize16(int value, std::vector<uint8_t>* data);
bool serialize16u(int value, std::vector<uint8_t>* data);
bool serialize8(int value, std::vector<uint8_t>* data);
bool serialize8u(int value, std::vector<uint8_t>* data);

#endif  // DS_QUADTREE_QUADTREE_SERIALIZATION_H
