/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by llindzey on Feb 5, 2020.
//

#ifndef DS_QUADTREE_QUADTREE_ROS_H
#define DS_QUADTREE_QUADTREE_ROS_H

#include <ros/ros.h>

namespace quadtree {

inline void RosFatalMessageCallback(const std::string &msg) {
  ROS_FATAL("%s", msg.c_str());
};
inline void RosErrorMessageCallback(const std::string &msg) {
  ROS_ERROR("%s", msg.c_str());
};
inline void RosWarnMessageCallback(const std::string &msg) {
  ROS_WARN("%s", msg.c_str());
};
inline void RosInfoMessageCallback(const std::string &msg) {
  ROS_INFO("%s", msg.c_str());
};
inline void RosDebugMessageCallback(const std::string &msg) {
  ROS_DEBUG("%s", msg.c_str());
};

}  // namespace quadtree

#endif  // DS_QUADTREE_QUADTREE_ROS_H
