### Overview

Coxploration is a NOAA-funded project that will use acoustic communications
to transfer scientifically-actionable data from the subsea robot to the topside
scientists during a dive, enabling the robot to be retargeted mid-mission.

In the initial demonstration on the 2020 Resing Sentry cruise, this data will
include:

* scalar science data: dORP hits, CTD, Oxygen, etc.
* multibeam maps, at varying resolutions
* images: selected using Sunshine topic modeling and transmitted using the progressive_imagery library.

This repository contains (most of) the various utilities/packages that
are being developed to support the coexploration project:

* [comms_manager](comms_manager/README.md): provides an acoustic ROS API

* [image_utils](image_utils/README.md): scripts and GUIs supporting use of Progressive imagery and Sunshine.

* [quadtree](quadtree/README.md): tools for incrementally sending a multibeam map

* coexploration_msgs: Any coexploration-specific messages

The top-level [launch](launch/README.md) directory contains example ROS
launch files for testing components of coexploration in simulation.

Some work for the project is also being done as navg3 plugins in the [navg3 repo](https://bitbucket.org/whoidsl/navg3-git):

* navg/plugins/science_data: displays the contents of SDQ31 (A2D2, CTD, paro depth, oxygen) in map view

### Architecture

The comms_manager provides the acoustic ROS API that all other utilities use for communication.
It is extensible to work with other modems, but all testing to date has been using a
micromodem.

![Alt text](design_docs/coexploration_architecture.png)

The above diagrom shows the software components that are involved in the planned demo of Coexploration on the 2020-resing cruise, along with
their current status.
This rest of this section will describe their functionality at a high level; see individual
packages for more detailed information.


##### Communications Infrastructure

* **TDMA GUI** - Allows the user to specify TDMA slots for both the topside and subsea modem.
  I don't yet consider this feature complete because changing between different
  TDMA cycles mid-dive requires significant operator expertise to reason about valid transitions.
* **Comms Manager Configuration GUI** - Allows the user to change which ROS messages should be
  relayed to and from the vehicle (default configuration is specified in launch file).
* **Comms Manager** - enables acoustic transmission of arbitrary ROS messages (so long as the
  topic and type are specified in a lookup table, and so long as the encoded message is <241 bytes).
  Has been tested repeatedly at sea; however, I do not consider it feature complete because
  it does not perform any compression (would be nice to do goby-style annotations on messages)
  and because it does not yet support service calls / remote procedure calls.
* **Micromodem Driver** - coexploration uses the ds_acomms_modem package. It handles runtime
  communication with the modem, but does not yet support configuration.

##### Multibeam Map Transfer

* **Quadtree Grid GUI** - displays the multi-resolution data that has been received topside and
  allows the user to select regions to be transmitted at a higher resolution. A standalone pyqt
  implementation exists, but the intention is for this functionality to be integrated into navg3
  so the maps can be visualized alongside other geospatial data.
* **Quadtree Receiver** - reconstructs a quadtree representation of the map based on the packets
  that have been received. Also mediates the user's region requests in order to transmit them
  along with the information required to avoid re-sending data.
* **Quadtree Sender** - subscribes to `/tf` and multibeam data; generates map; transfers it in
  frame-sided chunks.
* **Multibeam Driver** - map generation has been tested both with data from a Norbit and a Kongsberg.

##### Image Transfer

* **Progressive Imagery GUI** Shows all images that have been transferred, as well as allows the user to prioritize which images to finish sending first. 
* **Progressive imagery** A pair of nodes transfer JPEG2000 encoded images. The subsea node will transfer any images that are placed in the tx directory.
* **Photo Processing** On receipt of a timestamp from topside, this finds the closest image on disk, performs debayering and color correction, then puts it in the folder for progressive imagery to transfer.
* **Camera Driver** The legacy driver saved bayer-encoded images to disk; this was the input for the photoprocessing node, and has been tested during a dive. The new driver also publishes the images as ROS messages; this has not been tested as inputs for Yogi's Sunshine Topic Modeling.
* **Sunshine** (Yogi's Topic Modeling library): This will be used to provide the topside user with low-dimensional information about the images (either perplexity or categorization) that will inform which images should be selected for the bandwidth-intensive transfer.


##### Scalar Science Data

* **Scalar Science GUI** There is a plugin to navg3 that uses a gradient layer to plot the SDQ31 messages. Future work is to generalize this to plot any scalar message with a timestamp (basically, a map view equivalent to rqt_plot)
* **Comms Manager** Can be used to subsample any desired science data, for plotting in rqt_plot.
* **Aggregator** Currently, the SDQ31 queue aggregates data from common sensors and sends it up using the legacy queue manager (works in parallel with coexploration)
* **Jakuba Detector** The ORP data makes no sense when it is subsampled -- instead, it needs to be processed onboard and the top N hits transmitted acoustically. Jakuba has a Matlab implementation; porting it to ROS should be straightforward.
* **Drivers** All drivers already exist and have been running on Sentry since the ROS upgrade.
