#! /usr/bin/env python2.7

# Copyright 2019 Woods Hole Oceanographic Institution
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


'''
Node that uses the machinery of the comms_manager to print
all decoded received messages to the terminal.

Hacky way of monitoring vehicle comms, meant to be analogous
to the existing sdyne and umodem terminals.
'''
from __future__ import print_function, division

import rospy
import rospkg
import six

import ds_acomms_msgs.msg  # For MicromodemData

import comms_manager


class CommsEchoer(object):
    def __init__(self, filename=None):
        if filename is None:
            filename = rospy.get_param('/topic_lookup_filename')
        self.topic_lookup = comms_manager.TopicLookup(filename)

    def setup(self):
        self.modem_sub = rospy.Subscriber('rx_packet',
                                          ds_acomms_msgs.msg.MicromodemData,
                                          self.modem_data_callback)

    def modem_data_callback(self, msg):
        print("New message. Rate: {}, nframes: {}".format(msg.rate,
                                                          len(msg.frames)))
        idx = 0
        nf = 0
        for frame in msg.frames:
            nf += 1
            print("...{}-th frame!".format(nf))
            messages = comms_manager.decode(frame.payload,
                                            self.topic_lookup.msg_classes)
            for code, message in six.iteritems(messages):
                msg_str = message.__repr__()
                msg_str.replace('\n', ' ')
                idx += 1
                print("......{}-th message! Code: {} topic: {}"
                      .format(idx, ord(code), self.topic_lookup.topics[code]))


if __name__ == "__main__":
    rospy.init_node('comms_echoer')
    cm = CommsEchoer()
    cm.setup()
    rospy.spin()
