#! /usr/bin/env python2

import os
os.environ['TZ'] = 'UTC'
import rospy
import time

import std_msgs.msg



time_format = "%Y/%m/%d %H:%M:%S"

def get_posix(timestring):
    try:
        timestamp = time.strptime(timestring, time_format)
        tt = time.mktime(timestamp)
        return tt
    except:
        return None

def main():
    pub = rospy.Publisher("/transmit/sentry/coexploration/image_time", std_msgs.msg.Time, queue_size=1)

    while True:
        resp = raw_input("Please enter desired timestamp (yyyy/mm/dd hh:mm:ss): ")
        print "got {}".format(resp)

        posix = get_posix(resp)
        if posix is None:
            print "Could not convert input time: {}".format(resp)
            continue

        send = raw_input("Send image request for {} -> {}? (y/n)".format(resp, posix))
        if send.upper() == "Y":
            msg = std_msgs.msg.Time()
            msg.data.secs = posix
            pub.publish(msg)
            print "published!"
        else:
            print "not publishing"


if __name__ == "__main__":
    rospy.init_node("topside_image_requester")
    main()
