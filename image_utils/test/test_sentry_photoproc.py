#! /usr/bin/env python2

"""
If I'm going to run a python script on Sentry, it had better
be well tested =)

Since it uses ssh/scp, I'm going to set up directories on dpa that
have images (or not) in them. All paths relative to
~/progressive_imagery_test.

0) test0 - DOES NOT EXIST. Tests that remote directory not existing doesn't cause failure.

1) test1 - empty directory

2) test2 - single image
timestamps is:
sentry.20181023.055741941685.4689.tif -> 1540274261

  * image will only be copied once, even if multiple requests are made.
  * fails gracefully with nonexistant local dir

3) test3 - two images, timestamps:
timestamps are:
sentry.20181023.055741941685.4689.tif -> 1540274261
sentry.20181023.055748608229.4690.tif -> 1540274268
# Whoops -- i picked these to have an equidistant center, then didn't put them in the right spot on DPA :-\
sentry.20181023.060111939144.4744.tif -> 1540274471
sentry.20181023.060115272477.4745.tif -> 1540274475

  test requesting timestamps that are:
  * < first 1540274470
  * == first 1540274471
  * in between but closer to 1st 1540274472
  * exactly in the middle 1540274473
  * in between but closer to 2nd 1540274474
  * == second  1540274475
  * > second 1540274476

4) test4 - 20k images (from sentry 543), for testing performance try with symlink first? There were a LOT of predive checks, but for the main survey, timestamps are:
sentry.20191030.223609830228.12.tif -> 1572474969
sentry.20191101.031416356090.809.tif -> 1572578056

"""
import os
import rospy
import rosparam
import unittest

import std_msgs.msg  # for Time message

import image_utils


#remote_server = "sentry@dpa"
remote_server = "llindzey@llindzey-t480s"

#remote_path = "/home/sentry/progressive_imagery_test"
remote_path = "/home/llindzey/progressive_imagery_test"

class TestSetup(unittest.TestCase):
    def setUp(self):
        self.params = {"local_raw_dir": "/home/llindzey/progressive_imagery_test/raw_dir",
                       "local_proc_dir": "/home/llindzey/progressive_imagery_test/proc_dir",
                       "remote_server": "sentry@dpa",
                       "remote_dir": '/'.join([remote_path, "test0"])}

    def test_unset_params(self):
        '''
        Construction of the image fetcher/processer should fail if
        required params are not set.
        '''
        try:
            rosparam.delete_param("{}/local_raw_dir".format(rospy.get_name()))
            rosparam.delete_param("{}/local_proc_dir".format(rospy.get_name()))
            rosparam.delete_param("{}/remote_server".format(rospy.get_name()))
            rosparam.delete_param("{}/remote_dir".format(rospy.get_name()))
        except Exception as ex:
            # If the parameter wasn't set, then we're fine ...
            pass

        with self.assertRaises(KeyError):
            img_fetcher = image_utils.ImageFetcher()

    def test_nonexistant_raw_dir(self):
        '''
        Construction of the image fetcher/processor should fail if
        the specified local directories don't exit.
        '''
        self.params["local_raw_dir"] = "bad_raw_dir"
        rosparam.upload_params(rospy.get_name(), self.params)
        with self.assertRaises(Exception):
            img_fetcher = image_utils.ImageFetcher()

    def test_nonexistant_proc_dir(self):
        '''
        Construction of the image fetcher/processor should fail if
        the specified local directories don't exit.
        '''
        self.params["local_proc_dir"] = "bad_proc_dir"
        rosparam.upload_params(rospy.get_name(), self.params)
        with self.assertRaises(Exception):
            img_fetcher = image_utils.ImageFetcher()

    def test_construction(self):
        '''
        Test that construction succeeds with good parameters.
        (First step before finding way to break it...)
        '''
        rosparam.upload_params(rospy.get_name(), self.params)
        img_fetcher = image_utils.ImageFetcher()
        self.assertTrue(True)

class TestListImages(unittest.TestCase):
    '''
    These serve to both test list images AND to test that the remote
    server is set up correctly for the rest of the tests.
    '''
    def setUp(self):
        self.params = {"local_raw_dir": "/home/llindzey/progressive_imagery_test/raw_dir",
                       "local_proc_dir": "/home/llindzey/progressive_imagery_test/proc_dir",
                       "remote_server": remote_server}

    def test_list_images0(self):
        '''
        Test that list_images succeeds when the remote directory doesn't exist.
        '''
        self.params["remote_dir"] = '/'.join([remote_path, "test0"])
        rosparam.upload_params(rospy.get_name(), self.params)
        img_fetcher = image_utils.ImageFetcher()
        filenames = img_fetcher.list_images()
        self.assertEquals(0, len(filenames))

    def test_list_images1(self):
        '''
        Test that list_images succeeds when the remote directory is empty.
        '''
        self.params["remote_dir"] = '/'.join([remote_path, "test1"])
        rosparam.upload_params(rospy.get_name(), self.params)
        img_fetcher = image_utils.ImageFetcher()
        filenames = img_fetcher.list_images()
        self.assertEquals(0, len(filenames))

    def test_list_images2(self):
        '''
        Test that list_images succeeds when the remote directory doesn't exist.
        '''
        self.params["remote_dir"] = '/'.join([remote_path, "test2"])
        rosparam.upload_params(rospy.get_name(), self.params)
        img_fetcher = image_utils.ImageFetcher()
        filenames = img_fetcher.list_images()
        self.assertEquals(1, len(filenames))

    def test_list_images3(self):
        '''
        Test that list_images succeeds when the remote directory doesn't exist.
        '''
        self.params["remote_dir"] = '/'.join([remote_path, "test3"])
        rosparam.upload_params(rospy.get_name(), self.params)
        img_fetcher = image_utils.ImageFetcher()
        filenames = img_fetcher.list_images()
        self.assertEquals(2, len(filenames))

    def test_list_images4(self):
        '''
        Test that list_images succeeds when the remote directory doesn't exist.
        '''
        self.params["remote_dir"] = '/'.join([remote_path, "test4"])
        rosparam.upload_params(rospy.get_name(), self.params)
        img_fetcher = image_utils.ImageFetcher()
        filenames = img_fetcher.list_images()
        self.assertEquals(20513, len(filenames))

class TestCopyImage(unittest.TestCase):
    '''
    Tests edge cases related to copying image from remote server.
    '''
    def setUp(self):
        self.params = {"local_raw_dir": "/home/llindzey/progressive_imagery_test/raw_dir",
                       "local_proc_dir": "/home/llindzey/progressive_imagery_test/proc_dir",
                       "remote_server": remote_server}
        # File that exists in the test2 directory
        self.filename = "sentry.20181023.055741941685.4689.tif"
        try:
            os.remove("{}/{}".format(self.params["local_raw_dir"], self.filename))
        except OSError:
            # It won't always have been created ...
            pass

    def test_copy_image0(self):
        '''
        Trying to copy an image from a folder that doesn't exist.
        (Really, this should never be called since the nonexistant remote
        dir would be caught by list_images)
        '''
        self.params["remote_dir"] = '/'.join([remote_path, "test0"])
        rosparam.upload_params(rospy.get_name(), self.params)
        img_fetcher = image_utils.ImageFetcher()
        success = img_fetcher.copy_image(self.filename)
        self.assertFalse(success)

    def test_copy_image1(self):
        '''
        Trying to copy a filename that doesn't exist in the specified
        remote directory.
        (Again, we don't actually expect this to happen, since the
        filenames come directly from a `ls` on that remote directory...)
        '''
        self.params["remote_dir"] = '/'.join([remote_path, "test1"])
        rosparam.upload_params(rospy.get_name(), self.params)
        img_fetcher = image_utils.ImageFetcher()
        success = img_fetcher.copy_image(self.filename)
        self.assertFalse(success)

    def test_copy_image2(self):
        self.params["remote_dir"] = '/'.join([remote_path, "test2"])
        rosparam.upload_params(rospy.get_name(), self.params)
        img_fetcher = image_utils.ImageFetcher()
        success = img_fetcher.copy_image(self.filename)
        self.assertTrue(success)
        self.assertTrue(self.filename in os.listdir(self.params["local_raw_dir"]))

        # Second time through, the file is already there so shouldn't be copied again
        second_success = img_fetcher.copy_image(self.filename)
        self.assertFalse(second_success)


class TestTimeCallback(unittest.TestCase):
    '''
    All of these could also be handled by a rostest actually publishing
    the message, but ... I'm not worried about testing that the interface
    is plumbed properly. I want to make sure time->image mapping works, and
    this easy.
    '''
    def setUp(self):
        self.params = {"local_raw_dir": "/home/llindzey/progressive_imagery_test/raw_dir",
                       "local_proc_dir": "/home/llindzey/progressive_imagery_test/proc_dir",
                       "remote_server": remote_server,
                       "remote_dir": '/'.join([remote_path, "test3"])} # default to dir with two images.

        for filename in os.listdir(self.params["local_raw_dir"]):
            os.remove("{}/{}".format(self.params["local_raw_dir"], filename))

    def test_missing_dir(self):
        self.params["remote_dir"] = '/'.join([remote_path,"test0"])
        rosparam.upload_params(rospy.get_name(), self.params)

        img_fetcher = image_utils.ImageFetcher()
        msg = std_msgs.msg.Time()
        msg.data.secs = 1540274261  # sentry503 image 4689

        img_fetcher.time_callback(msg)

        # Should be no errors, and no images in the directory
        self.assertEquals(0, len(os.listdir(self.params["local_raw_dir"])))

    def test_empty_dir(self):
        self.params["remote_dir"] = '/'.join([remote_path,"test1"])
        rosparam.upload_params(rospy.get_name(), self.params)

        img_fetcher = image_utils.ImageFetcher()
        msg = std_msgs.msg.Time()
        msg.data.secs = 1540274261  # sentry503 image 4689

        img_fetcher.time_callback(msg)

        # Should be no errors, and no images in the directory
        self.assertEquals(0, len(os.listdir(self.params["local_raw_dir"])))


    def test_single_image_dir(self):
        self.params["remote_dir"] = '/'.join([remote_path,"test2"])
        rosparam.upload_params(rospy.get_name(), self.params)

        img_fetcher = image_utils.ImageFetcher()
        msg = std_msgs.msg.Time()
        filename = "sentry.20181023.055741941685.4689.tif"
        msg.data.secs = 1540274261  # sentry503 image 4689

        img_fetcher.time_callback(msg)

        # Should be no errors, and no images in the directory
        self.assertEquals(1, len(os.listdir(self.params["local_raw_dir"])))
        self.assertTrue(filename in os.listdir(self.params["local_raw_dir"]))


class TestTimeCallbackTwoImages(unittest.TestCase):
    '''
    Exhaustive set of before/between/after timestamps.
    Also meant to test the time conversion; if it's off, this will fail.
    (Of course, unless it's off in both places in the same way ...)
    '''
    def setUp(self):
        self.params = {"local_raw_dir": "/home/llindzey/progressive_imagery_test/raw_dir",
                       "local_proc_dir": "/home/llindzey/progressive_imagery_test/proc_dir",
                       "remote_server": remote_server,
                       "remote_dir": '/'.join([remote_path, "test3"])}

        for filename in os.listdir(self.params["local_raw_dir"]):
            os.remove("{}/{}".format(self.params["local_raw_dir"], filename))

        # These are separated by an even number of seconds, for nicer test_center
        # But, I forgot to copy them onto DPA
        #self.filename1 = "sentry.20181023.060111939144.4744.tif"
        #self.timestamp1 = 1540274471
        #self.filename2 = "sentry.20181023.060115272477.4745.tif"
        #self.timestamp2 = 1540274475

        self.filename1 = "sentry.20181023.055741941685.4689.tif"
        self.timestamp1 = 1540274261
        self.filename2 = "sentry.20181023.055748608229.4690.tif"
        self.timestamp2 = 1540274268

        rosparam.upload_params(rospy.get_name(), self.params)
        self.img_fetcher = image_utils.ImageFetcher()


    def test_before_first(self):
        '''
        This checks the boundary cases for which image is selected.
        '''
        msg = std_msgs.msg.Time()
        msg.data.secs = self.timestamp1 - 10
        self.img_fetcher.time_callback(msg)
        self.assertEquals(1, len(os.listdir(self.params["local_raw_dir"])))
        print self.filename1
        print os.listdir(self.params["local_raw_dir"])
        self.assertTrue(self.filename1 in os.listdir(self.params["local_raw_dir"]))


    def test_equal_first(self):
        '''
        This checks the boundary cases for which image is selected.
        '''
        msg = std_msgs.msg.Time()
        msg.data.secs = self.timestamp1
        self.img_fetcher.time_callback(msg)
        self.assertEquals(1, len(os.listdir(self.params["local_raw_dir"])))
        self.assertTrue(self.filename1 in os.listdir(self.params["local_raw_dir"]))

    def test_after_first(self):
        '''
        This checks the boundary cases for which image is selected.
        '''
        msg = std_msgs.msg.Time()
        msg.data.secs = self.timestamp1 + 1
        self.img_fetcher.time_callback(msg)
        self.assertEquals(1, len(os.listdir(self.params["local_raw_dir"])))
        self.assertTrue(self.filename1 in os.listdir(self.params["local_raw_dir"]))


    def test_center(self):
        '''
        This checks the boundary cases for which image is selected.
        '''
        msg = std_msgs.msg.Time()
        msg.data.secs = (self.timestamp1 + self.timestamp2)/2
        self.img_fetcher.time_callback(msg)
        self.assertEquals(1, len(os.listdir(self.params["local_raw_dir"])))
        self.assertTrue(self.filename1 in os.listdir(self.params["local_raw_dir"]))
    def test_before_second(self):
        '''
        This checks the boundary cases for which image is selected.
        '''
        msg = std_msgs.msg.Time()
        msg.data.secs = self.timestamp2-1
        self.img_fetcher.time_callback(msg)
        self.assertEquals(1, len(os.listdir(self.params["local_raw_dir"])))
        self.assertTrue(self.filename2 in os.listdir(self.params["local_raw_dir"]))
    def test_equal_second(self):
        '''
        This checks the boundary cases for which image is selected.
        '''
        msg = std_msgs.msg.Time()
        msg.data.secs = self.timestamp2
        self.img_fetcher.time_callback(msg)
        self.assertEquals(1, len(os.listdir(self.params["local_raw_dir"])))
        self.assertTrue(self.filename2 in os.listdir(self.params["local_raw_dir"]))
    def test_after_second(self):
        '''
        This checks the boundary cases for which image is selected.
        '''
        msg = std_msgs.msg.Time()
        msg.data.secs = self.timestamp2+10
        self.img_fetcher.time_callback(msg)
        self.assertEquals(1, len(os.listdir(self.params["local_raw_dir"])))
        self.assertTrue(self.filename2 in os.listdir(self.params["local_raw_dir"]))


if __name__ == "__main__":
    rospy.init_node("test_sentry_photoproc")
    unittest.main()
